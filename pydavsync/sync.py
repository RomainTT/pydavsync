# coding: utf-8

import pathlib

from pydavsync.webdav import connect_webdav, is_webdav_path
from pydavsync.pull import pull
from pydavsync.push import push
from pydavsync.tools import url_to_path
from pydavsync.exceptions import (
    MissingDavAddress,
    LocalPathDoesNotExist,
    DirectoryToFileError,
)


def sync(
    source,
    dest,
    src_user=None,
    src_pass=None,
    dst_user=None,
    dst_pass=None,
    ignore=None,
    insecure=False,
    delete=False,
    dry=False,
    verbose=False,
):
    """Entrypoint of pydavsync

    Returns:
        A 3-tuple of integers being:
            - the number of files actually transfered
            - the number of files already synchronized
            - the number of deleted files or directories
    """
    # TODO make custom exception for auth error, with details (src or dest?)
    if is_webdav_path(source):
        if is_webdav_path(dest):
            return sync_from_webdav_to_webdav(
                source,
                dest,
                src_user,
                src_pass,
                dst_user,
                dst_pass,
                ignore,
                insecure,
                delete,
                dry,
                verbose,
            )
        else:
            return sync_from_webdav_to_local(
                source, dest, src_user, src_pass, ignore, insecure, delete, dry, verbose
            )
    elif is_webdav_path(dest):
        if pathlib.Path(source).exists():
            return sync_from_local_to_webdav(
                source, dest, dst_user, dst_pass, ignore, insecure, delete, dry, verbose
            )
        else:
            raise LocalPathDoesNotExist(source)
    else:
        raise MissingDavAddress()


def sync_from_local_to_webdav(
    source,
    dest,
    username=None,
    password=None,
    ignore=None,
    insecure=False,
    delete=False,
    dry=False,
    verbose=False,
):
    """Synchronization from a local path to a remote webdav server.

    Returns:
        A 3-tuple of integers being:
            - the number of files actually transfered
            - the number of files already synchronized
            - the number of deleted files or directories
    """
    dst_client = connect_webdav(dest, username, password, insecure)
    src_path = pathlib.Path(source)
    dst_path = url_to_path(dest)
    return push(
        dst_client,
        src_path,
        dst_path,
        delete=delete,
        ignore=ignore,
        dry=dry,
        verbose=verbose,
    )


def sync_from_webdav_to_local(
    source,
    dest,
    username=None,
    password=None,
    ignore=None,
    insecure=False,
    delete=False,
    dry=False,
    verbose=False,
):
    """Synchronization from remote webdav server to a local path.

    Returns:
        A 3-tuple of integers being:
            - the number of files actually transfered
            - the number of files already synchronized
            - the number of deleted files or directories
    """
    src_client = connect_webdav(source, username, password, insecure)
    src_path = url_to_path(source)
    dst_path = pathlib.Path(dest)
    return pull(
        src_client,
        src_path,
        dst_path,
        delete=delete,
        ignore=ignore,
        dry=dry,
        verbose=verbose,
    )


def sync_from_webdav_to_webdav(
    source,
    dest,
    src_user=None,
    src_pass=None,
    dst_user=None,
    dst_pass=None,
    ignore=None,
    insecure=False,
    delete=False,
    dry=False,
    verbose=False,
):
    """Synchronization between two remote webdav servers.

    Returns:
        A 3-tuple of integers being:
            - the number of files actually transfered
            - the number of files already synchronized
            - the number of deleted files or directories
    """
    src_client = connect_webdav(source, src_user, src_pass, insecure)
    dst_client = connect_webdav(dest, dst_user, dst_pass, insecure)
    src_path = url_to_path(source)
    dst_path = url_to_path(dest)

    # Perform the following check before downloading anything, as it would be useless.
    # If source path is a directory, the destination cannot be an existing file.
    if src_client.isdir(str(src_path)) and dst_client.isfile(str(dst_path)):
        raise DirectoryToFileError()

    # TODO: do not pull everything before pushing! Comparison must be made between
    # the servers before any data transfer. Some special functions will be
    # required.
    # TODO: when src and dst servers are the same, do not pull files locally!
    # webdav supports remote copy which is much more efficient.
    raise NotImplementedError
