# coding: utf-8

import pytest

import pydavsync


@pytest.mark.parametrize(
    ["path", "result"],
    [
        ["https://my.host/my/path", True],
        ["/local/path", False],
        ["http://unsecure.org/path", True],
        ["https://nopath.x", True],
        ["relative/path", False],
        ["//double//slash//in//path", False],
        ["nonsense/http://foo.bar", False],
        ["https://many.sub.domain.xyz", True],
    ],
)
def test_is_dav_path(path, result):
    assert pydavsync.webdav.is_webdav_path(path) == result
